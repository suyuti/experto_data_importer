var XLSX = require('xlsx');

var wb = XLSX.readFile("./test.xlsx");

var personellerSheet    = wb.Sheets["BİLGİLER"]

var EgitimDurumlari     = []
var Gorevler            = []
var Departmanlar        = []
var Okullar             = []
var Bolumler            = []
var Projeler            = []

function PersonelCozumle(sheet) {
    var adiColumn           = "B"
    var egitimDurumColumn   = "C"
    var okulColumn          = "D"
    var gorevColumn         = "E"
    var departmanColumn     = "J"
    var iseBaslamaColumn    = "F"
    var istenAyrilmaColumn  = "G"
    var ekipColumn          = "I" 

    var row = 2;
    while(true) {
        var adiCell             = sheet[adiColumn + row];
        var egitimDurumCell     = sheet[egitimDurumColumn + row];
        var gorevCell           = sheet[gorevColumn + row];
        var departmanCell       = sheet[departmanColumn + row];
        var iseBaslamaCell      = sheet[iseBaslamaColumn + row];
        var istenAyrilmaCell    = sheet[istenAyrilmaColumn + row];
        var ekipCell            = sheet[ekipColumn + row];
        var okulCell            = sheet[okulColumn + row];

        if (adiCell == undefined) break;

        var adi             = (adiCell ? adiCell.v : undefined);
        var egitimDurum     = (egitimDurumCell ? egitimDurumCell.v : undefined);
        var gorev           = (gorevCell ? gorevCell.v : undefined);
        var departman       = (departmanCell ? departmanCell.v : undefined);
        var iseBaslama      = (iseBaslamaCell ? iseBaslamaCell.w : undefined);
        var istenAyrilma    = (istenAyrilmaCell ? istenAyrilmaCell.w : undefined);
        var ekip            = (ekipCell ? ekipCell.v : undefined);
        var okulBolum       = (okulCell ? okulCell.v : undefined);

        if (EgitimDurumlari.includes(egitimDurum) == false) {
            EgitimDurumlari.push(egitimDurum)
        }

        if (Gorevler.includes(gorev) == false) {
            Gorevler.push(gorev)
        }

        if (departman != undefined && Departmanlar.includes(departman) == false) {
            Departmanlar.push(departman)
        }

        var parts = okulBolum.split('/')
        var okul = parts[0].trim()
        var bolum = parts[1].trim()

        if (okul != undefined && Okullar.includes(okul) == false) {
            Okullar.push(okul)
        }
        if (bolum != undefined && Bolumler.includes(bolum) == false) {
            Bolumler.push(bolum)
        }



        //console.log(istenAyrilma)
        row++;
    }
}

PersonelCozumle(personellerSheet)

Projeler = wb.SheetNames.filter(sn => {return sn.startsWith("Proje")})


console.log(EgitimDurumlari)
console.log(Gorevler)
console.log(Departmanlar)
console.log(Okullar)
console.log(Bolumler)
console.log(Projeler)
